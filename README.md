# PRISMA template

The LaTeX file [`prisma.tex`](prisma.tex) provides a basic template for a [PRISMA
figure](http://www.prisma-statement.org/), made available under a
[Creative Commons Attribution-NonCommercial 4.0 International
license](LICENSE).

The figure should look like the following if rendered correctly:

![Rendered PRISMA](prisma.png)
